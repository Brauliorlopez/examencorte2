
function change() {
    let cmbMonedaOrigen = document.getElementById('cmbMonedaOrigen');
    let cmbMonedaDestino = document.getElementById('cmbMonedaDestino');
    let destinomoneda = document.getElementById('destinomoneda');

    seleccion = cmbMonedaOrigen.value;
    opciones = "";
    if (seleccion != '1')
        opciones += "<option value='1'>Pesos Mexicanos</option>";
    if (seleccion != '2')
        opciones += "<option value='2'>Dolar Estado Unidense</option>";
    if (seleccion != '3')
        opciones += "<option value='3'>Dolar Canadience</option>";
    if (seleccion != '4')
        opciones += "<option value='4'>Euro</option>";
    cmbMonedaDestino.innerHTML = opciones;
}

function calcular() {
    let txtCantidad = document.getElementById('txtCantidad');
    let cmbMonedaOrigen = document.getElementById('cmbMonedaOrigen');
    let cmbMonedaDestino = document.getElementById('cmbMonedaDestino');
    let txtSubtotal = document.getElementById('txtSubtotal');
    let txtTotalComision = document.getElementById('txtTotalComision');
    let txtTotalPagar = document.getElementById('txtTotalPagar');

    if (txtCantidad.value != '') {
        cantidad = txtCantidad.value;
        subTotal = cantidad;
        switch (cmbMonedaOrigen.value) {
            case '1': {
                subTotal /= 19.85;
                break;
            }
            case '3': {
                subTotal /= 1.35;
                break;
            }
            case '4': {
                subTotal /= 0.99;
                break;
            }
        }
        switch (cmbMonedaDestino.value) {
            case '1': {
                subTotal *= 19.85;
                break;
            }
            case '3': {
                subTotal *= 1.35;
                break;
            }
            case '4': {
                subTotal *= 0.99;
                break;
            }
        }
        comision = subTotal * 0.03;

        txtSubtotal.value = subTotal.toFixed(6);
        txtTotalComision.value = comision.toFixed(6);
        txtTotalPagar.value = (subTotal + comision).toFixed(6);
    }
}
var subTotalGlobal = 0;
var totalComisionGlobal = 0;
var totalPagarGlobal = 0;
function registrar() {
    let txtCantidad = parseInt.document.getElementById('txtCantidad');
    let txtSubtotal = parseInt.document.getElementById('txtSubtotal');
    let txtTotalComision = parseInt.document.getElementById('txtTotalComision');
    let txtTotalPagar = parseInt.document.getElementById('txtTotalPagar');
    let lblSubtotal = parseInt.document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = parseInt.document.getElementById('lblTotalPagar');

    if (txtSubtotal.value != '' && txtTotalComision != '' && txtTotalPagar != '') {
        subTotalGlobal = parseFloat(txtSubtotal.value);
        totalComisionGlobal += parseFloat(txtTotalComision.value);
        totalPagarGlobal += parseFloat(txtTotalPagar.value);

        lblSubtotal.innerHTML = "$" + subTotalGlobal.toFixed(2);
        lblTotalComision.innerHTML = "$" + totalComisionGlobal.toFixed(2);
        lblTotalPagar.innerHTML = "$" + totalPagarGlobal.toFixed(2);

        txtSubtotal.value = '';
        txtTotalComision.value = '';
        txtTotalPagar.value = '';
        txtCantidad.value = '';
    }
}

function borrar() {
    subTotalGlobal = 0.0;
    totalComisionGlobal = 0.0;
    totalPagarGlobal = 0.0;

    let txtCantidad = document.getElementById('txtCantidad');
    let txtSubtotal = document.getElementById('txtSubtotal');
    let txtTotalComision = document.getElementById('txtTotalComision');
    let txtTotalPagar = document.getElementById('txtTotalPagar');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    txtSubtotal.value = '';
    txtTotalComision.value = '';
    txtTotalPagar.value = '';
    txtCantidad.value = '';
    lblSubtotal.innerHTML = '$$$';
    lblTotalComision.innerHTML = '$$$';
    lblTotalPagar.innerHTML = '$$$';
}
//terminacion